var gift_queue = []

chrome.extension.sendMessage({type: 'gifts'}, function(res){
    console.log('back: ' + JSON.stringify(res))
    gift_queue = res

    let ul = document.getElementById('gift_list')
        
    while (ul.firstChild) {
        ul.removeChild(ul.firstChild);
    }

    for(let i=0; i<gift_queue.length; i++){
        let li = document.createElement('li')
        let span = document.createElement('span')
        let a = document.createElement('a')
        a.href = `https://www.douyu.com/special/jump/${gift_queue[i].drid}`
        a.target = '_blank'
        a.appendChild(document.createTextNode(gift_queue[i].dn))

        let timer = document.createElement('span')

        timer.textContent = parseInt((gift_queue[i].bgl*60*1000 + gift_queue[i].time - Date.now())/1000)


        function update(gift, timer){
            setInterval(function(){
                let left = parseInt((gift.bgl*60*1000 + gift.time - Date.now())/1000)
                timer.textContent = left

                
            }, 1000)
        }

        span.appendChild(a)
        span.appendChild(timer)
        update(gift_queue[i], timer)

        li.appendChild(span)
        ul.appendChild(li)
    }

})

chrome.extension.onMessage.addListener(function(msg) {
    console.log("message recieved: " + msg);
});



/*
setInterval(function(){


}, 1000)

var port = chrome.extension.connect({
    name: "Sample Communication"
});
port.postMessage("Hi BackGround");
port.onMessage.addListener(function(msg) {
    console.log("message recieved: " + msg);
    port.postMessage("echo " + msg);
});
*/