function md5cycle(x, k) {
    var a = x[0],
        b = x[1],
        c = x[2],
        d = x[3];
    a = ff(a, b, c, d, k[0], 7, -680876936);
    d = ff(d, a, b, c, k[1], 12, -389564586);
    c = ff(c, d, a, b, k[2], 17, 606105819);
    b = ff(b, c, d, a, k[3], 22, -1044525330);
    a = ff(a, b, c, d, k[4], 7, -176418897);
    d = ff(d, a, b, c, k[5], 12, 1200080426);
    c = ff(c, d, a, b, k[6], 17, -1473231341);
    b = ff(b, c, d, a, k[7], 22, -45705983);
    a = ff(a, b, c, d, k[8], 7, 1770035416);
    d = ff(d, a, b, c, k[9], 12, -1958414417);
    c = ff(c, d, a, b, k[10], 17, -42063);
    b = ff(b, c, d, a, k[11], 22, -1990404162);
    a = ff(a, b, c, d, k[12], 7, 1804603682);
    d = ff(d, a, b, c, k[13], 12, -40341101);
    c = ff(c, d, a, b, k[14], 17, -1502002290);
    b = ff(b, c, d, a, k[15], 22, 1236535329);
    a = gg(a, b, c, d, k[1], 5, -165796510);
    d = gg(d, a, b, c, k[6], 9, -1069501632);
    c = gg(c, d, a, b, k[11], 14, 643717713);
    b = gg(b, c, d, a, k[0], 20, -373897302);
    a = gg(a, b, c, d, k[5], 5, -701558691);
    d = gg(d, a, b, c, k[10], 9, 38016083);
    c = gg(c, d, a, b, k[15], 14, -660478335);
    b = gg(b, c, d, a, k[4], 20, -405537848);
    a = gg(a, b, c, d, k[9], 5, 568446438);
    d = gg(d, a, b, c, k[14], 9, -1019803690);
    c = gg(c, d, a, b, k[3], 14, -187363961);
    b = gg(b, c, d, a, k[8], 20, 1163531501);
    a = gg(a, b, c, d, k[13], 5, -1444681467);
    d = gg(d, a, b, c, k[2], 9, -51403784);
    c = gg(c, d, a, b, k[7], 14, 1735328473);
    b = gg(b, c, d, a, k[12], 20, -1926607734);
    a = hh(a, b, c, d, k[5], 4, -378558);
    d = hh(d, a, b, c, k[8], 11, -2022574463);
    c = hh(c, d, a, b, k[11], 16, 1839030562);
    b = hh(b, c, d, a, k[14], 23, -35309556);
    a = hh(a, b, c, d, k[1], 4, -1530992060);
    d = hh(d, a, b, c, k[4], 11, 1272893353);
    c = hh(c, d, a, b, k[7], 16, -155497632);
    b = hh(b, c, d, a, k[10], 23, -1094730640);
    a = hh(a, b, c, d, k[13], 4, 681279174);
    d = hh(d, a, b, c, k[0], 11, -358537222);
    c = hh(c, d, a, b, k[3], 16, -722521979);
    b = hh(b, c, d, a, k[6], 23, 76029189);
    a = hh(a, b, c, d, k[9], 4, -640364487);
    d = hh(d, a, b, c, k[12], 11, -421815835);
    c = hh(c, d, a, b, k[15], 16, 530742520);
    b = hh(b, c, d, a, k[2], 23, -995338651);
    a = ii(a, b, c, d, k[0], 6, -198630844);
    d = ii(d, a, b, c, k[7], 10, 1126891415);
    c = ii(c, d, a, b, k[14], 15, -1416354905);
    b = ii(b, c, d, a, k[5], 21, -57434055);
    a = ii(a, b, c, d, k[12], 6, 1700485571);
    d = ii(d, a, b, c, k[3], 10, -1894986606);
    c = ii(c, d, a, b, k[10], 15, -1051523);
    b = ii(b, c, d, a, k[1], 21, -2054922799);
    a = ii(a, b, c, d, k[8], 6, 1873313359);
    d = ii(d, a, b, c, k[15], 10, -30611744);
    c = ii(c, d, a, b, k[6], 15, -1560198380);
    b = ii(b, c, d, a, k[13], 21, 1309151649);
    a = ii(a, b, c, d, k[4], 6, -145523070);
    d = ii(d, a, b, c, k[11], 10, -1120210379);
    c = ii(c, d, a, b, k[2], 15, 718787259);
    b = ii(b, c, d, a, k[9], 21, -343485551);
    x[0] = add32(a, x[0]);
    x[1] = add32(b, x[1]);
    x[2] = add32(c, x[2]);
    x[3] = add32(d, x[3]);
}

function cmn(q, a, b, x, s, t) {
    a = add32(add32(a, q), add32(x, t));
    return add32((a << s) | (a >>> (32 - s)), b);
}

function ff(a, b, c, d, x, s, t) {
    return cmn((b & c) | ((~b) & d), a, b, x, s, t);
}

function gg(a, b, c, d, x, s, t) {
    return cmn((b & d) | (c & (~d)), a, b, x, s, t);
}

function hh(a, b, c, d, x, s, t) {
    return cmn(b ^ c ^ d, a, b, x, s, t);
}

function ii(a, b, c, d, x, s, t) {
    return cmn(c ^ (b | (~d)), a, b, x, s, t);
}

function md51(s) {
    var txt = '';
    var n = s.length,
        state = [1732584193, -271733879, -1732584194, 271733878],
        i;
    for (i = 64; i <= s.length; i += 64) {
        md5cycle(state, md5blk(s.substring(i - 64, i)));
    }
    s = s.substring(i - 64);
    var tail = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    for (i = 0; i < s.length; i++)
        tail[i >> 2] |= s.charCodeAt(i) << ((i % 4) << 3);
    tail[i >> 2] |= 0x80 << ((i % 4) << 3);
    if (i > 55) {
        md5cycle(state, tail);
        for (i = 0; i < 16; i++)
            tail[i] = 0;
    }
    tail[14] = n * 8;
    md5cycle(state, tail);
    return state;
}

function md5blk(s) {
    var md5blks = [],
        i;
    for (i = 0; i < 64; i += 4) {
        md5blks[i >> 2] = s.charCodeAt(i) + (s.charCodeAt(i + 1) << 8) + (s.charCodeAt(i + 2) << 16) + (s.charCodeAt(i + 3) << 24);
    }
    return md5blks;
}
var hex_chr = '0123456789abcdef'.split('');

function rhex(n) {
    var s = '',
        j = 0;
    for (; j < 4; j++)
        s += hex_chr[(n >> (j * 8 + 4)) & 0x0F] + hex_chr[(n >> (j * 8)) & 0x0F];
    return s;
}

function hex(x) {
    return x.map(rhex).join('');
}

function md5(s) {
    return hex(md51(s));
}
var add32 = function(a, b) {
    return (a + b) & 0xFFFFFFFF;
};
if (md5('hello') != '5d41402abc4b2a76b9719d911017c592') {
    add32 = function(x, y) {
        var lsw = (x & 0xFFFF) + (y & 0xFFFF),
            msw = (x >> 16) + (y >> 16) + (lsw >> 16);
        return (msw << 16) | (lsw & 0xFFFF);
    };
}


class Fifo {
    constructor(size){
        let power = 1;
        while(power < size)
            power*=2

        this.buffer = new Uint8Array(power)

        this.size = power
        this.in   = 0
        this.out  = 0
    }

    put(data) {
        let data_len = data.byteLength

        if(data_len >= this.size - this.length()){
            throw new Error('buffer overflow')
            return 0
        }

        let len = Math.min(data_len, this.size - this.in + this.out)
        let in_off = this.in & (this.size - 1)
        let l = Math.min(len, this.size - in_off)
        
        data = new Uint8Array(data)

        this.buffer.set(data.slice(0, l), in_off)
        this.buffer.set(data.slice(l, data_len), 0)

        let new_in = (this.in + len) | 0

        if(new_in < this.in){
            console.log('wrap ')
        }

        this.in = new_in

        return len
    }

    get(len){
        let out = this.peek(len)
        this.out = (this.out + out.byteLength) | 0
        return out
    }

    peek(len){
        len = Math.min(len, this.in - this.out)
        let out = new Uint8Array(len)

        let out_off = this.out & (this.size - 1)
        let l = Math.min(len, this.size - out_off)

        out.set(this.buffer.slice(out_off, out_off + l), 0)
        out.set(this.buffer.slice(0, len - l), l)

        return out
    }

    length() {
        return this.in - this.out
    }
}


class Douyu {
    static loginreq(roomid) {
        let devid = "202CB962AC59075B964B07152D234B70"
        let rt = Math.round(new Date().getTime() / 1000);
        let loginreq = {
            type: 'loginreq',
            username: '',
            ct: 0,
            password: '',
            roomid: roomid,
            devid: devid,
            rt: rt,
            pt: 2,
            vk: md5(`${rt}r5*^5;}2#\${XF[h+;'./.Q'1;,-]f'p[${devid}`),
            ver: '20150929',
            aver: 'H5_2018021001beta',
            biz: '',
            stk: '',
            ltkid: ''
        };
        return Douyu.encode(loginreq)
    }

    static encode(data){
        return Object.keys(data).map(key => 
            `${key}@=${Douyu.filterEnc(data[key])}`
        ).join('/') + '/';
    }

    static filterEnc(s) {
        s = s.toString();
        s = s.replace(/@/g, '@A');
        return s.replace(/\//g, '@S');
    }

    static decode(buf){
        let dec = new TextDecoder('utf-8')
        buf = dec.decode(buf)

        let l = 0
        let r = 0
        let ret = {}
        while((r = buf.indexOf("/", l)) >= 0){
            let div = buf.indexOf("@=", l)
            let k = buf.slice(l, div)
            let v = buf.slice(div + 2, r)
            ret[k.toString()] = v
            l = r + 1
        }
        return ret
    }

    static pack(data){
        let en = new TextEncoder()
        let len = data.length
        
        let buf = new Uint8Array(len + 13)
        let view = new DataView(buf.buffer)
    
        view.setUint16(0, len + 9, true)
        view.setUint16(4, len + 9, true)
        view.setUint16(8, 689, true)
        view.setUint16(10, 0)
        buf.set(en.encode(data), 12)
    
        return buf
    }

    constructor(room) {
        self = this
        self.room_id = room
        self.ws = new WebSocket('wss://danmuproxy.douyu.com:8501/')
        //self.ws = new WebSocket('wss://wsproxy.douyu.com:6673/')
        
        self.ws.binaryType = 'arraybuffer'
        self.fifo = new Fifo(65536)
        self.keepalive = {}
        self.onmsg = null

        self.ws.onopen = function open() {
            self.ws.send(Douyu.pack(Douyu.loginreq(room)));
            self.ws.send(Douyu.pack(`type@=joingroup/rid@=${room}/gid@=-9999/`));
        
            self.keepalive = setInterval(function(){
                //console.log('keeplive')
                self.ws.send(Douyu.pack(`type@=keeplive/tick@=${Math.floor(Date.now() / 1000)}/`))
            }, 40000)
        }

        self.ws.onmessage = function incoming(e) {
            let data = e.data

            self.fifo.put(data)
            while(true){
                let obj = self.process()
                if(obj){
                    self.onmsg(obj)
                }
                else{
                    break
                }
            }
        }

        self.ws.onerror = function(){
            console.log('error close')
            self.ws.close()
            clearInterval(self.keepalive)
            
        }

        self.ws.onclose = function(){
            console.log('close')
            clearInterval(self.keepalive)
        }
    }

    process() {
        let head = this.fifo.peek(12)
        if(head.length < 8){
            return null
        }

        let view = new DataView(head.buffer)
        let len1 = view.getUint16(0, true)
        let len2 = view.getUint16(4, true)

        if(len1 != len2 || len1 > 65536){
            throw new Error('wrong length')
        }

        if(this.fifo.length() < len1 + 4){
            return null
        }

        let pkg = this.fifo.get(len1 + 4)

        let body = pkg.slice(12, len1 + 3)

        let obj = Douyu.decode(body)
        return obj
    }

    close() {
        try{
            this.ws.close()
        }
        catch{
            return
        }
        
    }
}



function log_peck(gift, nums){
    let now = new Date().getTime() + 3600000 * 8
    let day =  (Math.floor(now/8.64e7)).toString()

    let peck = {gift: gift, nums: nums}

    console.log('log '+ day)

    chrome.storage.local.get([day], (result)=>{
        if(Object.keys(result).length == 0){
            let record = [ peck ]
            chrome.storage.local.set({[day]: record})
        }
        else{
            result.push(peck)
            chrome.storage.local.set({[day]: result})
        }
        console.log(result)
    })
}

/*

                if('txt' in obj){
                    console.log(obj.txt)
                }
                else if(obj.type == 'pingreq'){
                    console.log(obj)
                }
                else{
                    console.log(obj.type)
                }

                break
                */
