'use strict';

let gift_queue = []
let watcher = null

let last_update = Date.now()
let popup_port = null


chrome.webRequest.onBeforeSendHeaders.addListener(
    function(details) {
      for (var i = 0; i < details.requestHeaders.length; ++i) {
        if (details.requestHeaders[i].name === 'Origin') {
          details.requestHeaders[i].value = 'https://www.douyu.com/'
        }

      }
      return {requestHeaders: details.requestHeaders};
    },
    {urls: ["<all_urls>"]},
    ["blocking", "requestHeaders"]
);

chrome.extension.onMessage.addListener(function(msg, sender, response){
    console.log('recieved ' + JSON.stringify(msg))
    switch(msg.type){
    case 'gifts':
        clear_gifts()
        response(gift_queue)
        break
    case 'captcha':
        let url = 'res/bell.mp3'
        let audio = new Audio(url)
        audio.play()
        break
    case 'peck':
        let gift = msg.gift
        let nums = msg.nums
        log_peck(gift, nums)
        break
    }
    
})

chrome.runtime.onConnect.addListener(function(port) {
    console.log('connected from' + port)
})


function clear_gifts(){
    gift_queue = gift_queue.filter((obj)=>{
        return obj.time + obj.bgl*60*1000 > Date.now()
    })
}

function start_watching(){
    fetch('http://open.douyucdn.cn/api/RoomApi/live/DOTA2')
    .then(function(response) {
        if (response.status !== 200) {
            console.log('Looks like there was a problem. Status Code: ' +
            response.status);
            return;
        }

        // Examine the text in the response
        response.json().then(function(data) {
            let room = data.data[0].room_id
            console.log(`logging ${room}`)
            watcher = new Douyu(room)

            watcher.onmsg = function(msg){
            last_update = Date.now()

                if(msg.type == 'spbc'){
                    last_update = Date.now()

                    msg.time = Date.now()
                    gift_queue.push(msg)
                    console.log("append " + JSON.stringify(msg))
                    console.log(gift_queue)
                }
                else if(msg.type == 'chatmsg'){
                    //console.log(msg.txt)
                }
                else if(msg.type == 'rss'){
                    watcher.close()
                    setTimeout(start_watching, 2000)
                }
                else{
                    console.log(msg.type)
                }
            }
        });
    })
    .catch(function(err) {
        console.log('Fetch Error :-S', err);
    })
}

/*
function load_audio(){
    var audio_setting = [
        {
            url: 'res/bell.mp3'
        }
    ]
    var r = []

    for(let i of audio_setting){
        var audio = new Audio()
        
        audio.addEventListener("canplaythrough", function() {
            console.log('can play through') 
        }, false);
    

        audio.src = chrome.extension.getURL(i.url)
        audio.load()
        r.push(audio)
    }
    return r
}


*/



start_watching()

setInterval(function(){
    if(Date.now() - last_update > 3*60*1000){
        if(watcher){
            watcher.close()
        }
        start_watching()
    }

}, 30000)

setInterval(function(){
    clear_gifts()
}, 5000)