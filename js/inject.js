console.log("inject script")
var port = chrome.runtime.connect({name: "knockknock"});

function visible( elem ) {
    return !!( elem.offsetWidth || elem.offsetHeight || elem.getClientRects().length );
};

function check_peck(){
    //console.log('heart beat')
    let peck = document.querySelector(".peck")
    let peckCdn = document.querySelector(".peck-cdn")
    let peckCheck = document.querySelector(".peck-check-box")

    if(peckCdn && visible(peckCdn) && peckCdn.textContent === "领取" && !visible(peckCheck)){
        (function(peckC, peckCdnC){
            setTimeout(function(){
                console.log("click!!")
                
                var enter = new MouseEvent('mouseover', {
                    view: window,
                    bubbles: true,
                    cancelable: true
                    });
                peckC.dispatchEvent(enter)
                
                var click = new MouseEvent('click', {
                    view: window,
                    bubbles: true,
                    cancelable: true
                    });
                            
                peckCdnC.dispatchEvent(click)

            }, Math.ceil(Math.random() * 100))
        })(peck, peckCdn)

        return 'pending'
    }
    else{
        
        return 'done'
    }
}

function pooling(func, timeout){
    function helper(){
        func()
        setTimeout(function(){
            helper()     
        }, timeout)
    }

    helper()
}

pooling(check_peck, 500)


setTimeout(()=>{
    let ele = document.querySelector("div.chat")
    if(!ele){
        return
    }

    // Options for the observer (which mutations to observe)
    var config = { attributes: true, childList: true, subtree: true };

    // Callback function to execute when mutations are observed
    var callback = function(mutationsList) {
        for(var mutation of mutationsList) {
            if (mutation.type == 'childList') {
                if(mutation.addedNodes.length <= 0){
                    return
                }
                
                let add_ele = mutation.addedNodes[0]
                
                if(add_ele.nodeType != Node.ELEMENT_NODE){
                    return
                }

                if(add_ele.className === 'LI'){
                    return
                }

                if(!add_ele.classList.contains('peck-back-tip')){
                    return 
                }

                if(add_ele.classList.contains('peck-back-success')){
                    let regex = /派送的(\d+)个(.*)~/
                    let r = regex.exec(add_ele.textContent)
                    let msg = {
                        type: 'peck',
                        gift: r[2],
                        nums: r[1]
                    }
                    chrome.extension.sendMessage(msg, function(res){
                        
                    })

                    console.log(r[1] + ' x ' + r[2])
                }
                if(add_ele.classList.contains('peck-back-error')){
                    console.log('error')
                }

                
            }
            
            else if (mutation.type == 'attributes') {
                let ele = mutation.target
                if(ele.classList.contains('peck-check-box')){
                    if(visible(document.getElementById('geetest-box'))){
                        let msg = {
                            type: 'captcha'
                        }
                        chrome.extension.sendMessage(msg, function(res){
                            
                        })
                    }

                }

                console.log('The ' + mutation.target.className + ' attribute was modified.');
            }
            
        }
    };


    var observer = new MutationObserver(callback);
    observer.observe(ele, config);
}, 500)